clear all
global holes
%%---------------define parameters---------------------
level=3;%refinement level

qvec=[0:0.1:1 1.1:0.1:10]; %good to start from 0 as this provides a good guess
[q1,q2]=meshgrid(qvec);

[ix,iy]=meshgrid(1:length(qvec));
ind=rot90(ix+sqrt(-1)*iy);

%this creates a snaking index system
%helps with initial guesses for eigensolver
IND=[];flip=-1;
for k=-length(qvec)+1:length(qvec)-1
    dk=diag(ind,k);
    if flip==-1
        IND=[IND;flipud(dk)];
    else
        IND=[IND;dk];
    end
    flip=-flip;
end 
%define geometry
fem.problem=1;
holes.cx=0;holes.cy=0;holes.num=1;
holes.r=0.1; %max value: 0.5 DILUTE

%%---------------problem setup---------------------
fem=genmesh(fem,level);
assemb_elliptic
disp(['Problem size = ',num2str(N),'.'])
disp(['Range for q =[',num2str(min(qvec)),',',num2str(max(qvec)),']. Nq = ', num2str(length(qvec)),'.'])
%%-----------------------iterate-------------------------------------
fprintf('-----------------------------------------------------\n');
fprintf('r\t    (q1, q2)\t       f(qi) \n');
fprintf('-----------------------------------------------------\n');
tic

LQ=zeros(size(q1)); 
lq=0;

%choice of eigensolver
echoice=1;

LQ(1,1)=0;V(1,1,:)=ones(1,1,length(K.P*K.M));

for k=2:length(IND)
    
    i=real(IND(k));j=imag(IND(k));
    q=[q1(i,j); q2(i,j)];
    
    K.A=K.A0+q(1)*K.S1+q(2)*K.S2;
    
    etol=1e-12;eguess=lq;
    
    esolve
    
    if flag,disp('Negative eigenvalue!');break; end
    
    lq=lam;
    
    v1=K.P*v1;%full eigenvector
    
    flagv=0;value=v1(1);
    
    if value<0
        index = find(v1 > 0, 1, 'first');
    else 
        index = find(v1 < 0, 1, 'first');
    end    
        
    if length(index)~=0, disp('Sign change of eigenvector!');flagv=1;break;end
    
    LQ(i,j)=lq+norm(q)^2; 
    V(i,j,:)=v1;
    
end
fprintf('-----------------------------------------------------\n');
toc

%%-------------------plots of f(q) and V1-----------------------------
if flag==0 %if no issues, plot!
    figure(77),surf(q1,q2,LQ);rotate3d on;
    box on;grid on;xlabel('q1');ylabel('q2');zlabel('f(q)');   
end
