## Eigenvalue FEM code

This code is provided to support the paper *Diffusion in arrays of obstacles: beyond homogenisation* by Yahya Farah, Daniel Loghin, Alexandra Tzella and Jacques Vanneste.

---
## Description

* runeig.m -- main file: calls all the other files in this directory
* genmesh.m -- generates the geometry described in the paper and
  triangulates it
* assemb_elliptic -- FEM assembly file: pre-assembles the matrices
arising in the eigenvalue calculation
* esolve.m -- standard shift-and-invert routine

---

## Dependencies

The matlab files included in this directory require the Matlab PDE toolbox. All other dependencies are included as local files.

---

## Contributors

* Daniel Loghin d.loghin@bham.ac.uk
* Alexandra Tzella a.tzella@bham.ac.uk
